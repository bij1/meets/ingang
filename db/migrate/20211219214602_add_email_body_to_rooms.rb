class AddEmailBodyToRooms < ActiveRecord::Migration[6.0]
  def change
    add_column :rooms, :email_body, :string
  end
end
