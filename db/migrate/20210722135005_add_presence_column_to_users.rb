class AddPresenceColumnToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :presence, :boolean
  end
end
