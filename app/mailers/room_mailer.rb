class RoomMailer < ApplicationMailer

  def invite
    @room = Room.find(params[:room_id])
    @user = params[:user]
    @url = params[:url]
    mail(to: "\"#{ @user.name }\" <#{ @user.email }>", subject: @room.email_subject) do |format|
      # format.html { render 'another_template' }
      format.text { render inline: @room.email_body }
    end
  end
end
