class RoomsController < ApplicationController
  http_basic_authenticate_with name: Rails.application.config.admin_name,
                               password: Rails.application.config.admin_password

  before_action :set_room, only: [:show, :edit, :update, :destroy, :present]

  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = Room.all
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
  end

  # GET /rooms/new
  def new
    @room = Room.new

    meetings = bbb_server.get_meetings[:meetings]
    if meetings.length > 0
      meeting = meetings.first
      @room.name = meeting[:meetingName]
      @room.meeting = meeting[:meetingID]
      @room.attendee_pw = meeting[:attendeePW]
      @room.moderator_pw = meeting[:moderatorPW]
    end
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = Room.new(room_params)

    respond_to do |format|
      if @room.save
        format.html { redirect_to rooms_url, notice: 'Room was successfully created.' }
        format.json { render :show, status: :created, location: @room }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to rooms_url, notice: 'Room was successfully updated.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room.destroy
    respond_to do |format|
      format.html { redirect_to rooms_url, notice: 'Room was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /rooms/1/aanwezig
  # GET /rooms/1/aanwezig.csv
  def present
    require 'csv'
    conference = Set.new()
    if bbb_server.is_meeting_running?(@room.meeting)
      bbb_server.get_meeting_info(@room.meeting, @room.moderator_pw)[:attendees].each do |attendee|
        conference.add(attendee[:userID].to_i)
      end
    end
    csv_data = CSV.generate do |csv|
      User.where(room_id: @room.id).each do |attendee|
        if not attendee.proxy and (attendee.presence or conference === attendee.id)
          if attendee.vote and voter = attendee
            csv << [voter.id, voter.email, voter.name]
          end
          User.where(email: attendee.email, vote: true, proxy: true, room_id: @room.id).each do |proxy|
            csv << [proxy.id, proxy.email, "gemachtigde voor #{proxy.name}"]
          end
        end
      end
    end

    respond_to do |format|
      format.csv { send_data csv_data, filename: "#{@room.name} #{Time.zone.now}.csv" }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def room_params
      params.require(:room).permit(:name, :meeting, :attendee_pw, :moderator_pw, :email_subject, :email_body)
    end
end
